# Ενδεικτική χρήση των blocks του πρόσθετου

## Οθόνη LCD

![](./img/blocks_lcd.png)

Παρατηρήσεις:

* Δεν υποστηρίζονται Ελληνικοί χαρακτήρες

* Υποστηρίζονται και οθόνες 16x2 (2 γραμμές - 16 χαρακτήρες ανά γραμμή) και 20x4 (4 γραμμές - 20 χαρακτήρες ανά γραμμή)

## Οθόνη OLED 0.96 ιντσών

![](./img/blocks_oled.png)

Παρατηρήσεις:

* Υποστηρίζονται όλες οι επιλογές που προσφέρει η βιβλιοθήκη

* Ζωγραφική με σχήματα (γραμμή, τετράγωνο, τρίγωνο και κύκλος)

* Μέγεθος γραμματοσειράς (1,2 και 3)

## Αισθητήρας θερμοκρασίας / υγρασίας / υψόμετρου / βαρομετρικής πίεσης BME280 I2C

![](./img/blocks_bme280.png)

## Χρήση του αισθητήρα απόστασης του κιτ R2 (αυτοκίνητο) από το κιτ S1

![](./img/blocks_distance_sensor.png)

Παρατηρήσεις:

* Η σύνδεση του αισθητήρα απόστασης του κιτ R2, πρέπει να γίνει στο pin D3/D4 του S1.

![](./img/s1-distance_sensor.png)

Ευχαριστώ τον συνάδελφο Χρήστο Δεσποινίδη (5ο Δημοτικό Κιλκίς) για την υπόδειξη.
