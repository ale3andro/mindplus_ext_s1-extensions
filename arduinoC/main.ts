\   
enum SIZE {
    //% block="29*29"
    1,
    //% block="58*58"
    2
}

enum DIGITAL_PORTS {
    //% block="D5"
    5,
    //% block="D6"
    6,
    //% block="D7"
    7,
    //% block="D8"
    8,
    //% block="D9"
    9
}

enum ANALOG_PORTS {
    //% block="A0"
    0,
    //% block="A1"
    1,
    //% block="A2"
    2,
    //% block="A3"
    3,
}

enum RELAY_STATES {
    //% block="κλείσε"
    HIGH,
    //% block="άνοιξε"
    LOW,
}

enum CHANGE_LINE_OPTIONS {
    //% block="μην αλλάξεις γραμμή"
    0,
    //% block="άλλαξε γραμμή"
    1
}

enum TEXT_SIZES {
    //% block="1"
    1,
    //% block="2"
    2,
    //% block="3"
    3
}

enum DIRECTIONS {
    //% block="δεξιά"
    0,
    //% block="αριστερά"
    1,
    //% block="διαγώνια δεξιά "
    2,
    //% block="διαγώνια αριστερά"
    3,
    //% block="τέλος"
    4
}

enum I2CADDRESSES {
    //% block="0x27 (LCD)"
    0,
    //% block="0x3C (OLED)"
    1
}   

enum BME280_INPUT_TYPES {
    //% block="Θερμοκρασία (C)"
    0,
    //% block="Υγρασία (%)"
    1,
    //% block="Υψόμετρο (m)"
    2,
    //% block="Βαρομετρική πίεση (Pa)"
    3,
} 

enum LINESCOLUMNS {
    //% block="2 γραμμές και 16 στήλες"
    0,
    //% block="4 γραμμές και 20 στήλες"
    1
}

enum RECTANGLETYPES {
    //% block="με στρογγυλές γωνίες"
    0,
    //% block="με κανονικές γωνίες"
    1
}

enum FILLTYPES {
    //% block="μη γεμίσεις χρώμα"
    0,
    //% block="γέμισε χρώμα"
    1
}

enum REMOTE_CODES {
    //% block="OK"
    0x40,
    //% block="Πάνω βέλος"
    0x46,
    //% block="Κάτω βέλος"
    0x15,
    //% block="Δεξί βέλος"
    0x43,
    //% block="Αριστερό βέλος"
    0x44,
    //% block="1"
    0x16,
    //% block="2"
    0x19,
    //% block="3"
    0xD,
    //% block="4"
    0xC,
    //% block="5"
    0x18,
    //% block="6"
    0x5E,
    //% block="7"
    0x8,
    //% block="8"
    0x1C,
    //% block="9"
    0x5A,
    //% block="0"
    0x52,
    //% block="*"
    0x42,
    //% block="#"
    0x4A,
}

//% color="#44406f" iconWidth=50 iconHeight=40
namespace alxS1 {

    //% block="Oθόνη I2C αρχικοποίηση στη διεύθυνση [I2C_Address] με: [LINECOLUMN]" blockType="command"
    //% I2C_Address.shadow="dropdown" I2C_Address.options="I2CADDRESSES" I2C_Address.defl="I2CADDRESSES.0"
    //% LINECOLUMN.shadow="dropdown" LINECOLUMN.options="LINESCOLUMNS" LINECOLUMN.defl="LINESCOLUMNS.0"
    export function initI2CLed(parameter: any, block: any) {
        let i2c_address = parameter.I2C_Address.code
        let lines_columns = parameter.LINECOLUMN.code
        if(Generator.board === 'arduino'){
            Generator.addInclude("Wire_library","#include <Wire.h>");
            Generator.addInclude("LiquidCrystal_I2C","#include <LiquidCrystal_I2C.h>");
            let f_i2caddress = '0x27';
            if (i2c_address==1) 
                f_i2caddress = '0x3C';
            let f_lines = 2;
            let f_columns = 16;
            if (lines_columns==1) {
                f_lines = 4;
                f_columns = 20;
            }
            Generator.addObject(`LiquidCrystal_I2C_object` ,`LiquidCrystal_I2C`,`alx_lcd(${f_i2caddress}, ${f_columns}, ${f_lines});`);
            Generator.addSetup(`init`, `alx_lcd.init();`);
            Generator.addSetup(`backlight`, `alx_lcd.backlight();`);
        }
    }

    //% block="Oθόνη I2C γράψε [MESSAGE] στη γραμμή [LINE]" blockType="command"
    //% MESSAGE.shadow="string" MESSAGE,defl="S1"
    //% LINE.shadow="range" LINE.defl="0" LINE.params.max="3"
    export function writeI2CLed(parameter: any, block: any) {
        let message = parameter.MESSAGE.code
        let line = parameter.LINE.code
        if(Generator.board === 'arduino'){
            Generator.addCode(`alx_lcd.setCursor(0, ${line});`)
            Generator.addCode(`alx_lcd.print(${message});`)
        }
    }

    //% block="Oθόνη I2C καθάρισε" blockType="command"
    export function clearI2CLed(parameter: any, block: any) {
        if(Generator.board === 'arduino'){
            Generator.addCode(`alx_lcd.clear();`);
        }
    }

    //% block="Oθόνη OLED αρχικοποίηση στη διεύθυνση [I2C_Address] με μέγεθος γραμμάτων: [TEXT_SIZE]" blockType="command"
    //% I2C_Address.shadow="dropdown" I2C_Address.options="I2CADDRESSES" I2C_Address.defl="I2CADDRESSES.1"
    //% TEXT_SIZE.shadow="dropdown" TEXT_SIZE.options="TEXT_SIZES" TEXT_SIZE.defl="TEXT_SIZES.2"
    export function initOLED(parameter: any, block: any) {
        let i2c_address = parameter.I2C_Address.code
        let text_size = parameter.TEXT_SIZE.code
        if(Generator.board === 'arduino'){
            let f_i2caddress = '0x27';
            if (i2c_address==1) 
                f_i2caddress = '0x3C';
            Generator.addInclude("SPI_library","#include <SPI.h>");
            Generator.addInclude("Wire_library","#include <Wire.h>");
            Generator.addInclude("Adafruit_GFX_library","#include <Adafruit_GFX.h>");
            Generator.addInclude("Adafruit_SSD1306_library","#include <Adafruit_SSD1306.h>");
            
            Generator.addObject(`OLED_Object` ,`Adafruit_SSD1306`,`display(128, 64, &Wire, -1);`);
            Generator.addSetup(`init`, `display.begin(SSD1306_SWITCHCAPVCC, ${f_i2caddress});`);
            Generator.addCode(`display.setTextSize(${text_size});`);
            Generator.addCode(`display.setTextColor(SSD1306_WHITE);`);
            Generator.addCode(`display.clearDisplay();`);
        }
    }

    //% block="Oθόνη OLED καθάρισε" blockType="command"
    export function clearOLED(parameter: any, block: any) {
        if(Generator.board === 'arduino'){
            Generator.addCode(`display.clearDisplay();`);
        }
    }

    //% block="Oθόνη OLED γράψε [MESSAGE] και [CHANGE_LINE]" blockType="command"
    //% MESSAGE.shadow="string" MESSAGE,defl="S1"
    //% CHANGE_LINE.shadow="dropdown" CHANGE_LINE.options="CHANGE_LINE_OPTIONS" CHANGE_LINE.defl="CHANGE_LINE_OPTIONS.0"
    export function writeOLDED(parameter: any, block: any) {
        let message = parameter.MESSAGE.code
        let change_line = parameter.CHANGE_LINE.code
        if(Generator.board === 'arduino'){
            if (change_line==0) 
                Generator.addCode(`display.print(${message});`);
            else
                Generator.addCode(`display.println(${message});`);
            Generator.addCode(`display.display();`);
        }
    }
    
    //% block="Oθόνη OLED γράψε [MESSAGE] στο σημείο [X1], [Y1]" blockType="command"
    //% MESSAGE.shadow="string" MESSAGE,defl="S1"
    //% X1.shadow="range" X1.defl="0" X1.params.max="128"
    //% Y1.shadow="range" Y1.defl="0" Y1.params.max="64"
    export function writeOLDEDatPoint(parameter: any, block: any) {
        let message = parameter.MESSAGE.code
        let x1 = parameter.X1.code
        let y1 = parameter.Y1.code
        if(Generator.board === 'arduino'){
            Generator.addCode(`display.setCursor(${x1}, ${y1});`);
            Generator.addCode(`display.print(${message});`);
            Generator.addCode(`display.display();`);
        }
    }

    //% block="Oθόνη OLED [DIRECTION] κύλιση" blockType="command"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="DIRECTIONS" DIRECTION.defl="DIRECTIONS.0"
    export function scrollOLED(parameter: any, block: any) {
        let direction = parameter.DIRECTION.code
        if(Generator.board === 'arduino'){
            if (direction==0) 
                Generator.addCode(`display.startscrollright(0x00, 0x0F);`);
            else if (direction==1) 
                Generator.addCode(`display.startscrollleft(0x00, 0x0F);`);
            else if (direction==2)
                Generator.addCode(`display.startscrolldiagright(0x00, 0x07);`);
            else if (direction==3)
                Generator.addCode(`display.startscrolldiagleft(0x00, 0x07);`);
            else
                Generator.addCode(`display.stopscroll();`);
        }

    }

    //% block="Oθόνη OLED σχεδίασε γραμμή με αρχή το [X1], [Y1] και τέλος το [X2], [Y2] " blockType="command"
    //% X1.shadow="range" X1.defl="0" X1.params.max="128"
    //% Y1.shadow="range" Y1.defl="0" Y1.params.max="64"
    //% X2.shadow="range" X2.defl="128" X2.params.max="128"
    //% Y2.shadow="range" Y2.defl="64" Y2.params.max="64"
    export function drawlineOLED(parameter: any, block: any) {
        let x1 = parameter.X1.code
        let y1 = parameter.Y1.code
        let x2 = parameter.X2.code
        let y2 = parameter.Y2.code
        if(Generator.board === 'arduino'){
            Generator.addCode(`display.drawLine(${x1}, ${y1}, ${x2}, ${y2}, SSD1306_WHITE);`);
            Generator.addCode(`display.display();`);
        }

    }

    //% block="Oθόνη OLED σχεδίασε ορθογώνιο [RECTANGLETYPE] με αρχή το [X1], [Y1] και τέλος το [X2], [Y2] και [FILL]" blockType="command"
    //% RECTANGLETYPE.shadow="dropdown" RECTANGLETYPE.options="RECTANGLETYPES" RECTANGLETYPE.def="RECTANGLETYPES.0"
    //% X1.shadow="range" X1.defl="0" X1.params.max="128"
    //% Y1.shadow="range" Y1.defl="0" Y1.params.max="64"
    //% X2.shadow="range" X2.defl="128" X2.params.max="128"
    //% Y2.shadow="range" Y2.defl="64" Y2.params.max="64"
    //% FILL.shadow="dropdown" FILL.options="FILLTYPES" FILL.def="FILLTYPES.0"
    export function drawrectangleOLED(parameter: any, block: any) {
        let type = parameter.RECTANGLETYPE.code
        let x1 = parameter.X1.code
        let y1 = parameter.Y1.code
        let x2 = parameter.X2.code
        let y2 = parameter.Y2.code
        let fill = parameter.FILL.code
        if(Generator.board === 'arduino'){
            if (type==0) {
                if (fill==0)
                    Generator.addCode(`display.drawRoundRect(${x1}, ${y1}, ${x2}, ${y2}, 10, SSD1306_WHITE);`);
                else
                    Generator.addCode(`display.fillRoundRect(${x1}, ${y1}, ${x2}, ${y2}, 10, SSD1306_WHITE);`);
            }
            else {
                if (fill==0)
                    Generator.addCode(`display.drawRect(${x1}, ${y1}, ${x2}, ${y2}, SSD1306_WHITE);`);
                else
                    Generator.addCode(`display.fillRect(${x1}, ${y1}, ${x2}, ${y2}, SSD1306_WHITE);`);
            }
            Generator.addCode(`display.display();`);
        }
    }

    //% block="Oθόνη OLED σχεδίασε κύκλο με κέντρο το [X1], [Y1], ακτίνα [R] και [FILL]" blockType="command"
    //% X1.shadow="range" X1.defl="0" X1.params.max="128"
    //% Y1.shadow="range" Y1.defl="0" Y1.params.max="64"
    //% R.shadow="range" R.defl="64" X2.params.max="128"
    //% FILL.shadow="dropdown" FILL.options="FILLTYPES" FILL.def="FILLTYPES.0"
    export function drawcircleOLED(parameter: any, block: any) {
        let x1 = parameter.X1.code
        let y1 = parameter.Y1.code
        let r = parameter.R.code
        let fill = parameter.FILL.code
        if(Generator.board === 'arduino'){
            if (fill==0)
                Generator.addCode(`display.drawCircle(${x1}, ${y1}, ${r}, SSD1306_WHITE);`);
            else
                Generator.addCode(`display.fillCircle(${x1}, ${y1}, ${r}, SSD1306_WHITE);`);
            Generator.addCode(`display.display();`);
        }
    }
    
    //% block="Oθόνη OLED σχεδίασε τρίγωνο με κορυφή 1: [X1], [Y1], κορυφή 2: [X2], [Y2], κορυφή 3: [X3],[Y3] και [FILL]" blockType="command"
    //% X1.shadow="range" X1.defl="0" X1.params.max="128"
    //% Y1.shadow="range" Y1.defl="0" Y1.params.max="64"
    //% X2.shadow="range" X2.defl="128" X2.params.max="128"
    //% Y2.shadow="range" Y2.defl=0" Y2.params.max="64"
    //% X3.shadow="range" X3.defl="64" X3.params.max="128"
    //% Y3.shadow="range" Y3.defl="64" Y3.params.max="64"
    //% FILL.shadow="dropdown" FILL.options="FILLTYPES" FILL.def="FILLTYPES.0"
    export function drawtriangleOLED(parameter: any, block: any) {
        let x1 = parameter.X1.code
        let y1 = parameter.Y1.code
        let x2 = parameter.X2.code
        let y2 = parameter.Y2.code
        let x3 = parameter.X3.code
        let y3 = parameter.Y3.code
        let fill = parameter.FILL.code
        if(Generator.board === 'arduino'){
            if (fill==0)
                Generator.addCode(`display.drawTriangle(${x1}, ${y1}, ${x2}, ${y2}, ${x3}, ${y3}, SSD1306_WHITE);`);
            else
                Generator.addCode(`display.fillTriangle(${x1}, ${y1}, ${x2}, ${y2}, ${x3}, ${y3}, SSD1306_WHITE);`);
            Generator.addCode(`display.display();`);
        }
    }

    //% block="Ρελέ στο pin [BUTTON] [RELAY_STATE]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.5"
    //% RELAY_STATE.shadow="dropdown" RELAY_STATE.options="RELAY_STATES" RELAY_STATE.defl="RELAY_STATES.LOW"
    export function setRelay(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        let state = parameter.RELAY_STATE.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalWrite(${button}, ${state});`);
        }
    }

    //% block="Διάβασε απόσταση (αισθητήρας του R2)" blockType="reporter"
    export function getDistance(parameter: any, block: any) {
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_URM","#include <DFRobot_URM10.h>");
            Generator.addObject(`DFRobot_URM` ,`DFRobot_URM10`,`alx_distance`);
            Generator.addCode(`(alx_distance.getDistanceCM(3, 4))`);
        }
    }

    //% block="Διάβασε υγρασία χώματος από το pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="ANALOG_PORTS" BUTTON.defl="ANALOG_PORTS.0"
    export function getPotensiometerLevel(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`analogRead(${button})`);
        }
    }

    //% block="Διάβασε στάθμη υγρού από το pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="ANALOG_PORTS" BUTTON.defl="ANALOG_PORTS.0"
    export function getLiquidLevel(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`analogRead(${button})`);
        }
    }

    //% block="IR (R3) ανάγνωση από το pin [PORT]" blockType="command"
    //% PORT.shadow="dropdown" PORT.options="DIGITAL_PORTS" PORT.defl="DIGITAL_PORTS.6"
    export function enableIR(parameter: any, block: any) {
        let port=parameter.PORT.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("DefineNEC","#define DECODE_NEC");
            Generator.addObject(`IRResultsInt` ,`uint16_t`,`key_value=0`);
            Generator.addInclude("Arduino_IRremote","#include <IRremote.hpp>");
            
            Generator.addSetup(`ir_setup0`, `IrReceiver.begin(${port}, ENABLE_LED_FEEDBACK);`);
            Generator.addCode('if (IrReceiver.decode()) { IrReceiver.resume(); key_value=IrReceiver.decodedIRData.command; } else { key_value=0;};')
        }
    }

    //% block="ΙR (R3) πατήθηκε το κουμπί [REMOTE_CODE];" blockType="boolean"
    //% REMOTE_CODE.shadow="dropdown" REMOTE_CODE.options="REMOTE_CODES" REMOTE_CODE.defl="REMOTE_CODES.551489775"
    export function isIRCodeEqualTo(parameter: any, block: any) {
        let remoteCode = parameter.REMOTE_CODE.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`key_value==${remoteCode}`);
        }
    }

    //% block="Αισθητήρας μαύρης γραμμής στο pin [PIN]. Πατάει γραμμή;" blockType="boolean"
    //% PIN.shadow="dropdown" PIN.options="DIGITAL_PORTS" PIN.defl="DIGITAL_PORTS.5"
    export function isIRCodeEqualTo(parameter: any, block: any) {
        let pin = parameter.PIN.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalRead(${pin})`);
        }
    }

    //% block="BME280 I2C αρχικοποίηση στη διεύθυνση 0x76" blockType="command"
    export function initBME280(parameter: any, block: any) {
        if(Generator.board === 'arduino'){
            Generator.addInclude(`DFRobot_BME280`,`#include "DFRobot_BME280.h"`);
            Generator.addObject(`DFRobot_BME280_object` ,`DFRobot_BME280_IIC`, `alx_bme280;`);
            Generator.addSetup(`DFRobot_BME280_setup`, `alx_bme280.begin(0x76);`);
        }
    }

    //% block="BME280 διάβασε [BME280_INPUTS]" blockType="reporter"
    //% BME280_INPUTS.shadow="dropdown" BME280_INPUTS.options="BME280_INPUT_TYPES" BME280_INPUTS.defl="BME280_INPUT_TYPES.0"
    export function getBME280Data(parameter: any, block: any) {
        let type = parameter.BME280_INPUTS.code;
        if(Generator.board === 'arduino'){
            if (type==1) {
                Generator.addCode(`alx_bme280.getHumidity()`);
            } else if (type==2) {
                Generator.addCode(`alx_bme280.getAltitude()`);
            } else if (type==3) {
                Generator.addCode(`alx_bme280.getPressure()`);
            } else {
                Generator.addCode(`alx_bme280.getTemperature()`);
            }
            
        }
    }

    function replace(str :string) {
        return str.replace("+", "");
    }
}

